FROM	r-base:4.2.1

RUN	apt-get update
RUN apt-get install -y --no-install-recommends build-essential \
    libxml2-dev \
    libssl-dev \
    libcurl4-openssl-dev

RUN	Rscript -e "install.packages(c('mrbin', 'RestRserve', 'jsonlite'), dependencies=TRUE, repos='http://cran.us.r-project.org')"

WORKDIR /app
COPY    src/ /app/src
VOLUME  /app/spectra
RUN	chmod +x /app/src/start_script.sh
RUN	chmod +rx /app/src/spectra_parser.r

EXPOSE  9203

ENTRYPOINT	["/app/src/start_script.sh"]
