suppressPackageStartupMessages(library(mrbin, quietly=TRUE))
suppressPackageStartupMessages(library(RestRserve, quietly=TRUE))
suppressPackageStartupMessages(library(stringi, quietly=TRUE))
suppressPackageStartupMessages(library(stringr, quietly=TRUE))
suppressPackageStartupMessages(library(jsonlite, quietly=TRUE))

options(warn=-1)
spectrum_subfolder = '/app/spectra/'
setwd(spectrum_subfolder)


app = Application$new(content_type = "text/plain", middleware = list(CORSMiddleware$new(routes="/NMR/v1", match="partial", id="CORSMiddleware")))

parse_bruker_spectrum <- function (query_id, pattern) {
    matching_dirs = list.files(path=".", pattern=paste0("^", pattern, query_id, "_.*"))
    if (length(matching_dirs) != 1) {return}
    refined_query_id = paste0(matching_dirs, "/10/pdata/1")
    
    current_spectrum = readBruker(paste0(spectrum_subfolder, refined_query_id), dimension="1D")
    ppm_array = as.numeric(rownames(as.data.frame(current_spectrum$currentSpectrum)))
    mag_array = as.numeric(current_spectrum$currentSpectrum)
    return(list(toJSON(ppm_array), toJSON(mag_array)))
}

app$add_route(
    path="/NMR/v1/parseSpectrum_Bruker",
    method="GET",
    FUN = function (request, response) {
        response$set_header("Allow", "GET")
        query_id = request$parameters_query["id"]
        pattern = request$parameters_query["pattern"]

        if (is.na(pattern) | pattern == "") {
          pattern="AKI_1_24_" 
        }
        parsing_res = parse_bruker_spectrum(query_id, pattern)
        print(paste0("Received raw spectry query for ID:", query_id, " with pattern ", pattern))

        response$set_content_type("application/json")
        response$set_body(toJSON(parsing_res, auto_unbox=T))        
    }
)

backend = BackendRserve$new()
backend$start(app, http_port = 9203)


